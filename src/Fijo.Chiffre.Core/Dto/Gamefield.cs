﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Fijo.Chiffre.Core.Dto {
	public enum Location {
		Top,
		Bottom,
		Middle,
		Right,
		Left
	}
	public enum Gamemode {
		Walking,
		Fight
	}
	public class Gamefield {
		public static IList<int> BarrierX = new List<int> {5, 6};
		public static IList<int> BarrierY = new List<int> {5, 6};

		public Field[,] Fields { get; set; }
		public IDictionary<Location, Barrier> Barriers { get; set; }
	}
	public class Field {
		public IList<Figure> Figures { get; set; }
	}
	public class FightField : Field {
		public Coordinate Position { get; set; }
	}
	public class Barrier : Field {
		
	}
	public class Zoll : Barrier {
		
	}
	public class Figure {
		public Field Field { get; set; }
	}
	public class WalkingFigure : Figure {
		
	}
	public class FightFigure : Figure {
			
	}
	public class Gun : FightFigure {
		
	}
	public class Knive : FightFigure {
		
	}
	public class Headphone : FightFigure {
		
	}
	public class BigBarrierFigure : FightFigure {
		
	}
	public class Leiter : BigBarrierFigure {
		
	}
	public class Drahtschere : BigBarrierFigure {
		
	}
	public class Pass : FightFigure {
		
	}
	public class Player {
		
	}

	public class Movement {
		public Figure TargetFigure { get; set; }
		public Field Destination { get; set; }
	}

	public class Coordinate {
		public int X { get; set; }
		public int Y { get; set; }
	}


	public enum FightDiceResults {
		Gerade,
		Schraeg,
		GeradeOrSchraeg,
		Point,
		Barrier
	}

	public class DiceResult {}
	public class FightDiceResult {
		public FightDiceResults Type { get; set; }
	}

	public class MovementService {
		public IEnumerable<Movement> GetPossibleMovements(Gamefield gamefield, Figure targetFigure) {
			return GetInternPossibleMovements(gamefield, (dynamic) targetFigure);
		}

		public IEnumerable<Movement> GetInternPossibleMovements(Gamefield gamefield, FightFigure targetFigure, FightDiceResult diceRes) {
			var field = targetFigure.Field;
			var fightField = field as FightField;
			if (fightField != null) {
				var position = fightField.Position;
				var fields = gamefield.Fields;
				switch (diceRes.Type) {
					case FightDiceResults.Barrier:
						if (targetFigure is BigBarrierFigure) {
							return Gamefield.BarrierX.Contains(position.X) || Gamefield.BarrierY.Contains(position.Y)
								? 

						}
						if (targetFigure is Pass) {
							return Gamefield.BarrierX.Contains(position.X) && Gamefield.BarrierY.Contains(position.Y)
								       ? gamefield.Barriers.OfType<Zoll>().Select(x => GetMovement(targetFigure, x))
								       : Enumerable.Empty<Movement>();
						}
				}
			}
		}

		protected Movement GetMovement(Figure figure, Field to) {
			return new Movement {TargetFigure = figure, Destination = to};
		}

		public IEnumerable<Coordinate> GetPossiblePositions(Coordinate position, FightDiceResult diceRes) {
			switch (diceRes.Type) {
				case FightDiceResults.Gerade:
					return GetPossibleGeradeMovements(position);
				case FightDiceResults.Schraeg:
					return GetPossibleSchraegMovements(position);
				case FightDiceResults.GeradeOrSchraeg:
					return GetPossibleGeradeMovements(position).Concat(GetPossibleSchraegMovements(position));
				default:
					throw new ArgumentException("invalid arg value", "diceRes");
			}
		}

		private IEnumerable<Coordinate> GetPossibleSchraegMovements(Coordinate position) {
			yield return new Coordinate {X = position.X - 1, Y = position.Y - 1};
			yield return new Coordinate {X = position.X - 1, Y = position.Y + 1};
			yield return new Coordinate {X = position.X + 1, Y = position.Y - 1};
			yield return new Coordinate {X = position.X + 1, Y = position.Y + 1};
		}

		private IEnumerable<Coordinate> GetPossibleGeradeMovements(Coordinate position) {
			yield return new Coordinate {X = position.X - 1, Y = position.Y};
			yield return new Coordinate {X = position.X, Y = position.Y - 1};
			yield return new Coordinate {X = position.X + 1, Y = position.Y};
			yield return new Coordinate {X = position.X, Y = position.Y + 1};
		}
	}
}